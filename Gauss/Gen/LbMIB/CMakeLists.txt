################################################################################
# Package: LbMIB
################################################################################
gaudi_subdir(LbMIB v7r0p2)

gaudi_depends_on_subdirs(Event/GenEvent
                         GaudiAlg
                         Kernel/PartProp
                         Kernel/LHCbKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(LbMIB
                 src/*.cpp
                 INCLUDE_DIRS AIDA
                 LINK_LIBRARIES GenEvent GaudiAlgLib LHCbKernel PartPropLib)

gaudi_install_python_modules()

