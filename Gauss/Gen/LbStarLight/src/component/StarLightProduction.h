#ifndef LBSTARLIGHT_STARLIGHTPRODUCTION_H
#define LBSTARLIGHT_STARLIGHTPRODUCTION_H 1

using namespace std;

// Beamtool declaration.
class IBeamTool;
class upcEvent;
class starlight;
class inputParameters;

/**
 * Wrapper for the Gaudi random number generator.
 *
 * @class  GaudiRandomForStarLight
 * @file   StarLightProduction.h
 * @author Mingrui Zhao, Philip Ilten
 * @date   2017-02-02
 */
class GaudiRandomForStarLight : public randomGenerator {
public:

  /// Initialize the Gaudi random number generator.
  StatusCode initialize(IRndmGenSvc *svc);

  /// Finalize the Gaudi random number generator.
  StatusCode finalize();

  /// Flat random function used by StarLight, argument is not used.
  double Rndom(int);

private:

  /// Internal Gaudi random number generator.
  Rndm::Numbers m_rndm;
};

/**
 * Production tool to generate events with StarLight.
 *
 * TO-DO.
 *
 * @class  StarLightProduction
 * @file   StarLightProduction.h
 * @author Mingrui Zhao, Philip Ilten
 * @date   2017-02-02
 */
class StarLightProduction : public GaudiTool, virtual public IProductionTool {
 public:
  typedef vector<string> CommandVector;

  /// Default constructor.
  StarLightProduction(const string &type, const string &name,
		      const IInterface *parent);

  /// Default destructor.
  virtual ~StarLightProduction();

  /**
   * Initialize the tool.
   *
   * First initialize the Gaudi tool and beam tool, then read the
   * default and user settings, and finally set the center-of-mass
   * energy.
   */
  virtual StatusCode initialize();

  /// Finalize the tool.
  virtual StatusCode finalize();

  /// Initialize the StarLight generator.
  virtual StatusCode initializeGenerator();

  /// Generate an event.
  virtual StatusCode generateEvent(HepMC::GenEvent *theEvent,
				   LHCb::GenCollision *theCollision);

  /**
   * Convert the StarLight HepEvt record to HepMC format.
   *
   * First dummy beams are added since the beam particles are not
   * available from StarLight. Then the particles are added and any
   * resonance is manually inserted of the process goes through a
   * resonance.
   */
  virtual StatusCode toHepMC (upcEvent &theUPCEvent, HepMC::GenEvent *theEvent);

  /**
   * Update particles properties.
   *
   * This only updates the particle properties which can be changed
   * within StarLight. The masses for 2112, 211, 111, 321, 11, 13, 15,
   * 9010221, 221, 331, 441, 225, 115, 335, 113, 223, 333, 443,
   * 100443, 553, 100553, and 200553 are set. Additionally, the widths
   * for 9010221, 221, 331, 441, 225, 115, 335, 113, 223, 333, 443,
   * 100443, 553, 100553, and 200553 are set. Finally, the spins for
   * 9010221, 221, 331, 441, 225, 115, and 335 set.
   **/
  virtual void updateParticleProperties(const LHCb::ParticleProperty *thePP);

  /// This method is not implemented.
  virtual void setStable(const LHCb::ParticleProperty *thePP);

  /// This method is not implemented.
  virtual void savePartonEvent(HepMC::GenEvent *theEvent);

  /// This method is not implemented.
  virtual void retrievePartonEvent( HepMC::GenEvent *theEvent);

  /// This method is not implemented.
  virtual StatusCode hadronize(HepMC::GenEvent *theEvent,
			       LHCb::GenCollision *theCollision);

  /// This method is not implemented.
  virtual void printRunningConditions();

  /// This method is not implemented.
  virtual bool isSpecialParticle(const LHCb::ParticleProperty *thePP) const;

  /// This method is not implemented.
  virtual StatusCode setupForcedFragmentation(const int thePdgId);

  /// This method is not implemented.
  virtual void turnOnFragmentation();

  /// This method is not implemented.
  virtual void turnOffFragmentation();

protected:

  /// Parse the StarLight settings.
  StatusCode parseSettings(const CommandVector &settings);

private:

  GaudiRandomForStarLight m_rndm;     ///< Random number generator wrapper.
  CommandVector    m_defaultSettings; ///< StarLight default settings.
  CommandVector    m_userSettings;    ///< StarLight user settings.
  bool             m_decays;          ///< Allow StarLight to perform the decay.
  int              m_tries;           ///< Number of StarLight tries.
  starlight        m_generator;       ///< The standalone generator.
  inputParameters  m_pars;            ///< Generator parameters.
  int              m_res;             ///< Resonance for the process.
};

#endif // LBSTARLIGHT_STARLIGHTPRODUCTION_H
