// Include files

// local
#include "DaughtersInLHCbAndCutsForDstarFromB.h"

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h"

// from Kernel
#include "Kernel/ParticleID.h"

// from Generators
#include "GenEvent/HepMCUtils.h"
#include "MCInterfaces/IDecayTool.h"

// from STL
#include <algorithm>

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCbAndCutsForDstarFromB
//
// 2018-03-30 Adam Morris
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DaughtersInLHCbAndCutsForDstarFromB )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DaughtersInLHCbAndCutsForDstarFromB::DaughtersInLHCbAndCutsForDstarFromB( const std::string& type,
                                                                          const std::string& name,
                                                                          const IInterface* parent )
  : DaughtersInLHCbAndCutsForDstar( type, name, parent ),
    m_fromBcuts     ( NULL                ) {
  declareInterface< IGenCutTool >( this ) ;
}

//=============================================================================
// Destructor
//=============================================================================
DaughtersInLHCbAndCutsForDstarFromB::~DaughtersInLHCbAndCutsForDstarFromB( ) { ; }

//=============================================================================
// initialize
//=============================================================================
StatusCode DaughtersInLHCbAndCutsForDstarFromB::initialize()
{
  const StatusCode sc = DaughtersInLHCb::initialize();
  if ( sc.isFailure() ) return sc;
  
  // load the tool to check if the signal is ultimately from a b
  m_fromBcuts = tool<IGenCutTool>( "SignalIsFromBDecay", this );

  return sc;
}

//=============================================================================
// Acceptance function
//=============================================================================
bool DaughtersInLHCbAndCutsForDstarFromB::applyCut( ParticleVector & theParticleVector ,
                                                    const HepMC::GenEvent * theEvent ,
                                                    const LHCb::GenCollision * theHardInfo )
  const
{
  return ( m_fromBcuts->applyCut(theParticleVector,theEvent,theHardInfo) && 
           DaughtersInLHCbAndCutsForDstar::applyCut(theParticleVector,theEvent,theHardInfo) );
}

