c Modified automatically with Gen/SuperChic2/python/SuperChic2.py.
c Do not manually modify. Generated from superchicv2.04/src/init/init.f.
      subroutine sbr_init
      implicit double precision(a-y)
      integer isurv
      character*100 dum

      include 'pi.f'
      include 'vars.f'
      include 'intag.f'
      include 'pdfinf.f'
      integer entryp,exitp
      character*100 iintag,ipdfname
      common/initcom/isurv,irts,iisurv,ipdfmember,entryp,exitp,iintag,
     &ipdfname,dum

      if (entryp.eq.1000) then
      goto 1000
      endif
      if (entryp.eq.1001) then
      goto 1001
      endif
      if (entryp.eq.1002) then
      goto 1002
      endif
      if (entryp.eq.1003) then
      goto 1003
      endif

 1000 continue
      if (exitp.eq.1000) then
      return
      endif
      pi=dacos(-1d0)

      rts = irts
      isurv = iisurv
      intag = iintag
      pdfname = ipdfname
      pdfmember = ipdfmember


 1002 continue
      if (exitp.eq.1002) then
      return
      endif
      read(*,*)dum
      read(*,*)dum
      read(*,*)dum
      read(*,*)rts
      read(*,*)isurv
      read(*,*)intag
      read(*,*)dum
      read(*,*)dum
      read(*,*)PDFname
      read(*,*)PDFmember
 1003 continue
      if (exitp.eq.1003) then
      return
      endif

cccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccc   Init LHAPDF
cccccccccccccccccccccccccccccccccccccccccccccccccccc

      call inpdf

cccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccc 

      call initpars(isurv)   ! Initialise soft survival parameters
      call calcop            ! proton opacity 
      call calcscreen        ! screening amplitude
      call initsud           ! sudakov factor
      call inithg            ! skewed PDF

      print*,'Now run ./superchic'

 1001 continue
      if (exitp.eq.1001) then
      return
      endif
      stop
      end
