#ifndef DELPHESTUPLE_H 
#define DELPHESTUPLE_H 1

// Include files 
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/GaudiTupleTool.h"

/** @class DelphesTuple DelphesTuple.h
 *  
 *
 *  @author Benedetto Gianluca Siddi
 *  @date   2015-10-20
 */
class DelphesTuple : public GaudiTupleAlg {
public: 
  /// Standard constructor
  DelphesTuple( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~DelphesTuple( ); ///< Destructor

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize  ();    ///< Algorithm execution

protected:

private:
  std::string m_particles;  ///< Location in TES of output smeared MCParticles.                                                                                                     
  std::string m_vertices;///< Location in TES of output smeared MCVertices.                                                                                                         
  std::string m_generatedParticles;//< Location in TES of output generated MCParticles.
  std::string m_generatedVertices;//< Location in TES of output generated MCVertices.                                                   
  std::string m_neutral_proto_location;//location of neutral protoparticles in tes.
  
  std::map<Long64_t, std::pair<LHCb::MCParticle*,LHCb::MCParticle*>> Pions;
  std::map<Long64_t, LHCb::MCParticle*> GenPions;  
  std::map<Long64_t, LHCb::MCParticle*> RecPions;  
  std::map<Long64_t, LHCb::ProtoParticle*>ClusteredPhotons;
  
  Tuples::Tuple tupleGen = NULL;
  Tuples::Tuple tupleRec = NULL;
  Tuples::Tuple tupleRes = NULL;
  Tuples::Tuple tupleResPhoton = NULL;

  
};
#endif // DELPHESHIST_H
