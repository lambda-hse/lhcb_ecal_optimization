#ifndef DELPHESALG_H 
#define DELPHESALG_H 1

// Include files 
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "TMath.h"
#include "Math/VectorUtil.h"

#include "TLorentzVector.h"
#include "TVector3.h"

class ExRootConfReader ;
class Delphes ;
class DelphesFactory ;
class TObjArray ;

/** @class DelphesAlg DelphesAlg.h
 *
 *  Interface to DELPHES packages
 *
 *  @author Patrick Robbe
 *  @date   2015-01-19
 */
class DelphesAlg : public GaudiAlgorithm {
public: 
  /// Standard constructor
  DelphesAlg( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~DelphesAlg( ); ///< Destructor

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize  ();    ///< Algorithm finalization
  /* virtual StatusCode fill( const LHCb::Particle* */
  /*                          , const LHCb::Particle* */
  /*                          , const std::string& */
  /*                          , Tuples::Tuple& ); */
  HepMC::GenVertex* primaryVertex(const HepMC::GenEvent* genEvent) const;
  
protected:

private:
  Long64_t key_gen; //sequential key to give to candidates
  
  std::string m_generationLocation; ///< Location in TES of input HepMC events.
  std::string m_particles; ///< Location in TES of output smeared MCParticles.
  std::string m_vertices; ///< Location in TES of output smeared MCVertices.
  std::string m_generatedParticles; //< Location in TES of output generated MCParticles.
  std::string m_generatedVertices;  //< Location in TES of output generated MCVertices.
  std::string m_LHCbDelphesCard;//< location of the file delphes_card_LHCb.tcl
  std::string m_track_location;//< location of tracks as defined from Delphes Card.
  /* std::map<Long64_t,LHCb::MCParticle*> Gen; */
  /* std::map<Long64_t,LHCb::MCParticle*> Rec; */

  ExRootConfReader * m_confReader;
  Delphes * m_modularDelphes;
  DelphesFactory * m_Delphes;
  TObjArray * m_allParticleOutputArray;
  TObjArray * m_stableParticleOutputArray;
  TObjArray * m_partonOutputArray;
  
  
};
#endif // DELPHESALG_H
