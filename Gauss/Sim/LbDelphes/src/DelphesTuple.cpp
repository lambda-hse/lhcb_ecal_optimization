// Include files 

 // from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "GaudiKernel/ToolFactory.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiAlg/GaudiTupleAlg.h"
// Event.
#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// local
#include "DelphesTuple.h"
#include "TLorentzVector.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DelphesTuple
//
// 2015-10-20 : Benedetto Gianluca Siddi
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( DelphesTuple )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DelphesTuple::DelphesTuple( const std::string& name,
                          ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator )
{
   declareProperty("MCFastParticleLocation",
                  m_particles = "/Event/MCFast/MCParticles",
                  "Location to write smeared MCParticles.");  declareProperty("MCFastVerticesLocation",
                  m_vertices = "/Event/MCFast/MCVertices",
                  "Location to write smeared MCVertices.");
  
  declareProperty("MCVerticesLocation",
                  m_generatedVertices = LHCb::MCVertexLocation::Default,
                  "Location to write generated MCVertices.");
  
  declareProperty("MCParticleLocation",
                  m_generatedParticles = LHCb::MCParticleLocation::Default,
                  "Location to write generated MCParticles.");
  
  declareProperty("NeutralProtoLocation",
                  m_neutral_proto_location = LHCb::ProtoParticleLocation::Neutrals,
                  "location of neutral protoparticles");
  
}
//=============================================================================
// Destructor
//=============================================================================
DelphesTuple::~DelphesTuple() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode DelphesTuple::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize();
  // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  tupleGen = nTuple("Gen", "gen_Tuple");
  tupleRec = nTuple("Rec", "rec_Tuple");
  tupleRes = nTuple("Res", "res_Tuple");
  tupleResPhoton = nTuple("ResPhoton","res_Photon_Tuple");
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  return StatusCode::SUCCESS;

}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DelphesTuple::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  // Get the run and event number from the MC Header
  LHCb::MCHeader* evt = get<LHCb::MCHeader>( LHCb::MCHeaderLocation::Default,IgnoreRootInTES);
  
  //Get TES container
  LHCb::MCParticles *m_genParticleContainer =
    get<LHCb::MCParticles>(m_generatedParticles);

  LHCb::MCVertices  *m_genVerticesContainer = 
    get<LHCb::MCVertices>(m_generatedVertices);

  LHCb::MCParticles *m_particleContainer = 
    get<LHCb::MCParticles>(m_particles);

  LHCb::MCVertices  *m_verticesContainer = 
    get<LHCb::MCVertices>(m_vertices);
  
  LHCb::ProtoParticles* m_neutralProtos = get<LHCb::ProtoParticles>(m_neutral_proto_location);
  
  GenPions.clear();
  RecPions.clear();
  Pions.clear();
  ClusteredPhotons.clear();
  

  
  LHCb::MCParticles::const_iterator i;
  for( i=m_genParticleContainer->begin(); i != m_genParticleContainer->end(); i++ ) 
  {
    GenPions.insert(std::pair<Long64_t,LHCb::MCParticle*>((*i)->key(),(*i)));    
    LHCb::MCParticle *P = (*i);
    const LHCb::MCVertex &Vtx = P->originVertex();
    const LHCb::ParticleID &ID = P->particleID(); 
    Double_t x = Vtx.position().X();
    Double_t y = Vtx.position().Y();
    Double_t z = Vtx.position().Z();
    // Double_t x = P->momentum().X();
    // Double_t y = P->momentum().Y();
    // Double_t z = P->momentum().Z();
    Double_t R = TMath::Power(x,2) + TMath::Power(y,2);
    R = TMath::Sqrt(R);
    Double_t eta = P->pseudoRapidity();
    Double_t phi = P->momentum().Phi();
    Double_t theta = P->momentum().Theta();
    Double_t px = P->momentum().Px();
    Double_t py = P->momentum().Py();
    Double_t pz = P->momentum().Pz();
    Double_t theta_x = px/pz;
    Double_t theta_y = py/pz;
    Double_t p = P->momentum().P();
    Int_t Pid = ID.pid();
    tupleGen->column( "runNumber",    evt->runNumber() );
    tupleGen->column( "eventNumber",  evt->evtNumber() );
    tupleGen->column("p",p);
    tupleGen->column("theta",theta);
    tupleGen->column("thetaX",theta_x);
    tupleGen->column("thetaY",theta_y);
    tupleGen->column("x",x);
    tupleGen->column("y",y);
    tupleGen->column("z",z);
    tupleGen->column("px",px);
    tupleGen->column("py",py);
    tupleGen->column("pz",pz);
    tupleGen->column("phi",phi);
    tupleGen->column("eta",eta);
    tupleGen->column("pid",Pid);    
    tupleGen->write();
    if(Pid==22){
      tupleResPhoton->column("mc_calo_key",(*i)->key());
      // auto v = P->endVertices().back();
      // std::cout<<v<<std::endl;
        
      //tupleResPhoton->column("mc_vertex_key",Vtx.key());
      
      tupleResPhoton->column("mc_calo_e",P->momentum().P());
      // tupleResPhoton->column("mc_calo_x",endVtx->position().x());
      // tupleResPhoton->column("mc_calo_y",endVtx->position().y());
      // tupleResPhoton->column("mc_calo_z",endVtx->position().z());
      
    }
    
  }
  // info()<<"GenSize: "<<GenPions.size()<<endmsg;

  for( i=m_particleContainer->begin(); i != m_particleContainer->end(); i++ ) 
  { 
    
    RecPions.insert(std::pair<Long64_t,LHCb::MCParticle*>((*i)->key(),(*i)));
    LHCb::MCParticle *P = (*i);
    const LHCb::MCVertex *Vtx = P->endVertices()[0];
    const LHCb::ParticleID &ID = P->particleID();
    
    Double_t x = Vtx->position().X();
    Double_t y = Vtx->position().Y();
    Double_t z = Vtx->position().Z();
    // Double_t x = P->momentum().X();
    // Double_t y = P->momentum().Y();
    // Double_t z = P->momentum().Z(); 
    Double_t eta = P->pseudoRapidity();
    Double_t phi = P->momentum().Phi();
    Double_t theta = P->momentum().Theta();
    Double_t R = TMath::Power(x,2) + TMath::Power(y,2);
    Double_t px = P->momentum().Px();
    Double_t py = P->momentum().Py();
    Double_t pz = P->momentum().Pz();
    Double_t theta_x = px/pz;
    Double_t theta_y = py/pz;
    Double_t p = P->momentum().P();
    Int_t Pid = ID.pid();
    
    
    R = TMath::Sqrt(R);
    tupleRec->column( "runNumber",    evt->runNumber() );
    tupleRec->column( "eventNumber",  evt->evtNumber() );
    tupleRec->column("p",p);
    tupleRec->column("theta",theta);
    tupleRec->column("thetaX",theta_x);
    tupleRec->column("thetaY",theta_y);
    tupleRec->column("x",x);
    tupleRec->column("y",y);
    tupleRec->column("z",z);
    tupleRec->column("px",px);
    tupleRec->column("py",py);
    tupleRec->column("pz",pz);
    tupleRec->column("phi",phi);
    tupleRec->column("eta",eta);
    tupleRec->column("pid",Pid);
    tupleRec->write();
  }
  // info()<<"RecSize: "<<RecPions.size()<<endmsg;  
  std::map<Long64_t, LHCb::MCParticle*>::iterator it;

  for(it=GenPions.begin(); it!=GenPions.end(); ++it)
  {
    if(RecPions.find(it->first) != RecPions.end()) 
      Pions.insert(std::make_pair(it->first,std::make_pair(it->second,RecPions[it->first])));
  }
  // info()<<"PionsSize: "<<Pions.size()<<endmsg;
  
  std::pair<LHCb::MCParticle*,LHCb::MCParticle*> particles;

  std::map<Long64_t, std::pair<LHCb::MCParticle*,LHCb::MCParticle*>>::iterator itp;

  LHCb::MCParticle *P1;
  LHCb::MCParticle *P2;
  LHCb::ParticleID *ID1;
  LHCb::ParticleID *ID2;
  Double_t Px1, Py1, Pz1, Pt1, x1, y1, z1, Pid1, E1, M1;
  Double_t Px2, Py2, Pz2, Pt2, x2, y2, z2, Pid2, E2, M2;
  for(itp=Pions.begin(); itp!=Pions.end(); ++itp)
  {
    particles = itp->second;
    P1 = (LHCb::MCParticle*)particles.first;
    P2 = (LHCb::MCParticle*)particles.second;
    const LHCb::ParticleID &ID1 = P1->particleID();
    const LHCb::ParticleID &ID2 = P2->particleID();
    const LHCb::MCVertex &Vtx1 = P1->originVertex();
    const LHCb::MCVertex *Vtx2 = P2->endVertices()[0];
    
    x1 = Vtx1.position().X();
    y1 = Vtx1.position().Y();
    z1 = Vtx1.position().Z();
    Pid1 = ID1.pid();
    Px1 = P1->momentum().Px();
    Py1 = P1->momentum().Py();
    Pz1 = P1->momentum().Pz();
    Pt1 = P1->momentum().Pt();
    E1 = P1->momentum().E();
    M1 = P1->momentum().M();

    Double_t eta1 = P1->pseudoRapidity();
    Double_t phi1 = P1->momentum().Phi();
    Double_t theta1 = P1->momentum().Theta();



    x2 = Vtx2->position().X();
    y2 = Vtx2->position().Y();
    z2 = Vtx2->position().Z();
    Pid2 = ID2.pid();
    Px2 = P2->momentum().Px();
    Py2 = P2->momentum().Py();
    Pz2 = P2->momentum().Pz();
    Pt2 = P2->momentum().Pt();
    E2 = P2->momentum().E();
    M2 = P2->momentum().M();
    Double_t p1 = P1->momentum().P();
    Double_t p2 = P2->momentum().P();
    
    Double_t theta_x1 = Px1/Pz1;
    Double_t theta_y1 = Py1/Pz1;
    Double_t theta_x2 = Px2/Pz2;
    Double_t theta_y2 = Py2/Pz2;

    Double_t eta2 = P2->pseudoRapidity();
    Double_t phi2 = P2->momentum().Phi();
    Double_t theta2 = P2->momentum().Theta();


    tupleRes->column( "runNumber",    evt->runNumber() );
    tupleRes->column( "eventNumber",  evt->evtNumber() );
    tupleRes->column("pDiff",p2-p1);
    tupleRes->column("thetaDiff",theta2-theta1);
    tupleRes->column("thetaXDiff",theta_x2-theta_x1);
    tupleRes->column("thetaYDiff",theta_y2-theta_y1);
    tupleRes->column("xDiff",x2-x1);
    tupleRes->column("yDiff",y2-y1);
    tupleRes->column("zDiff",z2-z1);
    tupleRes->column("pxDiff",Px2-Px1);
    tupleRes->column("pyDiff",Py2-Py1);
    tupleRes->column("pzDiff",Pz2-Pz1);
    tupleRes->column("phiDiff",phi2-phi1);
    tupleRes->column("etaDiff",eta2-eta1);
    tupleRes->column("pidDiff",Pid2-Pid1);

    tupleRes->column("pGen",p1);
    tupleRes->column("thetaGen",theta1);
    tupleRes->column("thetaXGen",theta_x1);
    tupleRes->column("thetaYGen",theta_y1);
    tupleRes->column("xGen",x1);
    tupleRes->column("yGen",y1);
    tupleRes->column("zGen",z1);
    tupleRes->column("pxGen",Px1);
    tupleRes->column("pyGen",Py1);
    tupleRes->column("pzGen",Pz1);
    tupleRes->column("phiGen",phi1);
    tupleRes->column("etaGen",eta1);
    tupleRes->column("pidGen",Pid1);

    tupleRes->column("pRec",p2);
    tupleRes->column("thetaRec",theta2);
    tupleRes->column("thetaXRec",theta_x2);
    tupleRes->column("thetaYRec",theta_y2);
    tupleRes->column("xRec",x2);
    tupleRes->column("yRec",y2);
    tupleRes->column("zRec",z2);
    tupleRes->column("pxRec",Px2);
    tupleRes->column("pyRec",Py2);
    tupleRes->column("pzRec",Pz2);
    tupleRes->column("phiRec",phi2);
    tupleRes->column("etaRec",eta2);
    tupleRes->column("pidRec",Pid2);

    tupleRes->write();

  }
  
  LHCb::ProtoParticles::const_iterator thingy;
  for(thingy=m_neutralProtos->begin(); thingy!=m_neutralProtos->end();thingy++){
    ClusteredPhotons.insert(std::make_pair((*thingy)->key(),(*thingy)));
    tupleResPhoton->column("energy",(*thingy)->calo().at(0)->e());
    tupleResPhoton->column("x",(*thingy)->calo().at(0)->position()->x());
    tupleResPhoton->column("y",(*thingy)->calo().at(0)->position()->y());
    tupleResPhoton->column("recoKey",(*thingy)->key());    
    tupleResPhoton->write();
    
  }
  
  

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode DelphesTuple::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}

//=============================================================================
