#
# Options specific for a given job
# ie. setting of random number seed and name of output files
#

from Gaudi.Configuration import *
from Configurables import Gauss, ApplicationMgr, UpdateManagerSvc
from Gauss.Configuration import *    


#importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam3500GeV-md100-2011-nu2.py")


#importOptions("/afs/cern.ch/lhcb/software/releases/GAUSS/GAUSS_v48r3/Sim/Gauss/options/Gauss-2012.py")
importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-mu100-2012-nu2.5.py")
from Configurables import LHCbApp
#LHCbApp().DDDBtag   = "dddb-20130929-1"
#LHCbApp().CondDBtag = "sim-20130522-1-vc-mu100"
LHCbApp().DDDBtag   = "dddb-20170721-3"
LHCbApp().CondDBtag = "sim-20170721-2-vc-md100"

#############################################################
#eventType = '56000400'#photon particle gun # was uncomment
eventType = '30000000'#min bias # was comment
############################################################


#eventType = '13102201'#Bs-> phi gamma
#eventType = '562000001'#pi0 particle gun

#LHCbApp().OutputLevel=DEBUG
#importOptions("Beam4000GeV-mu100-MayJun2012-nu2.5.py")
#importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-md100-2012-fix1.py")
importOptions('$DECFILESROOT/options/%s.py'%eventType)
#importOptions('./pi0particle_gun/%s.py'%eventType)
#importOptions("ParticleGunElectron.py")
#importOptions("ParticleGunPion.py")
#importOptions("ParticleGunPhoton.py")
#importOptions("ParticleGunMinBias.py")

#########################################################
#importOptions("$LBPGUNSROOT/options/PGuns.py") #was uncomment
#importOptions("$LBGENROOT/options/GEN.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py") #was comment
########################################################


#importOptions('$DECFILESROOT/options/11102013.py') #B0->pi+ pi-
#importOptions('$DECFILESROOT/options/56000400.py')#photon particle gun, don't forget to uncomment pgun



importOptions("$LBDELPHESROOT/options/LbDelphes.py")
from Configurables import ParticleGun
#from Configurables import FlatNParticles
#ParticleGun = ParticleGun("ParticleGun")
#from Configurables import MomentumRange
#ParticleGun.addTool(MomentumRange, name="MomentumRange")
#ParticleGun.ParticleGunTool = "MomentumRange"
#ParticleGun.EventType = 56000400
#ParticleGun.addTool(FlatNParticles, name="FlatNParticles")
#ParticleGun.NumberOfParticlesTool = "FlatNParticles"
##ParticleGun.FlatNParticles.MinNParticles = 2
##ParticleGun.FlatNParticles.MaxNParticles = 2
#ParticleGun.MomentumRange.PdgCodes = [-211, 211]

#from Configurables import DDDBConf, CondDB, LHCbApp
#env = dict(os.environ)
#LHCbApp().DDDBtag   = "dddb-20130503-1"
#DDDBConf(DataType = "2012")
#CondDB().UseLatestTags = ["2012"]

            
#--Number of events
nEvts = 100
LHCbApp().EvtMax = nEvts

#from Configurables import FakeEventTime, EventClockSvc
#EventClockSvc().EventTimeDecoder = 'FakeEventTime'

#--Generator phase, set random numbers
gaussGen = GenInit("GaussGen")
gaussGen.FirstEventNumber = 1
gaussGen.RunNumber        = 1082

#genMonitor = GaudiSequencer( "GenMonitor" )
#genMonitor.Members += [ "DumpHepMCTree/DumpSignal", "DumpHepMC/DumpAll"]
#delphesMonitor = GaudiSequencer( "DelphesMonitor" )
#delphesMonitor.Members += [ "PrintMCDecayTreeAlg/PrintMCInputDelphes",  "PrintMCDecayTreeAlg/PrintMCOutputDelphes" ]
#from Configurables import PrintMCDecayTreeAlg
#PrintMCDecayTreeAlg("PrintMCOutputDelphes").MCParticleLocation = "MCFast/MCParticles"
#PrintMCDecayTreeAlg("PrintMCOutputDelphes").MCParticleLocation = "Rec/Track/Best"
#PrintMCDecayTreeAlg("PrintMCOutputDelphes").MCParticleLocation = "Rec/ProtoP/Charged"
#PrintMCDecayTreeAlg("PrintMCOutputDelphes").MCParticleLocation = "Rec/ProtoP/Neutrals"


#--Set name of output files for given job and read in options
idFile = 'Gauss_'+str(eventType)
HistogramPersistencySvc().OutputFile = idFile+'_histos.root'
#--- Save ntuple with hadronic cross section information
ApplicationMgr().ExtSvc += [ "NTupleSvc" ]
NTupleSvc().Output = ["FILE1 DATAFILE='GaussTuple_{idFile}.root' TYP='ROOT' OPT='NEW'".format(idFile=eventType)]
       
FileCatalog().Catalogs = [ "xmlcatalog_file:Gauss_NewCatalog_0.xml" ]

#importOptions("/afs/cern.ch/lhcb/software/releases/LHCB/LHCB_v38r7/Det/DetDescChecks/options/LoadDDDB.py")


def writeHits():    
    OutputStream("GaussTape").ItemList.append("/Event/MC/Vertices#1")
    OutputStream("GaussTape").ItemList.append("/Event/MC/Particles#1")
    OutputStream("GaussTape").ItemList.append("/Event/MCFast/MCParticles#1")
    OutputStream("GaussTape").ItemList.append("/Event/MCFast/MCVertices#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/ProtoP/Charged#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/ProtoP/Neutrals#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Track/Best#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Rich/PIDs#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Calo/Photons#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Calo/EcalClusters#1")
    OutputStream("GaussTape").ItemList.append("/Event/MC/DigiHeader#1")
    OutputStream("GaussTape").ItemList.append("/Event/DAQ/RawEvent#1")
    OutputStream("GaussTape").ItemList.append("/Event/DAQ/ODIN#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Vertex/Primary#1")
    print(OutputStream("GaussTape"))
appendPostConfigAction(writeHits)

#from Configurables import SimConf
# def addItemsToTape():
#    simWriter = SimConf().writer()
#    simWriter.ItemList.append('/Event/Rec/ProtoP/Neutrals#1')
#    simWriter.ItemList.append("/Event/Rec/Vertex#1")
#    simWriter.ItemList.append("/Event/Rec/Vertex/Primary#1")
#    print simWriter
# appendPostConfigAction(addItemsToTape)
