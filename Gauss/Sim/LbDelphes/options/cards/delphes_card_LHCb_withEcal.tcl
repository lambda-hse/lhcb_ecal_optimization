#set MaxEvents 1000
#set RandomSeed 123

#/ author Zehua Xu , Tsinghua University , Email: Zehua.Xu@cern.ch

#  PhotonEnergySmearing
#  PhotonEfficiency

#  TowerMerger
#  EFlowMerger 

#######################################
# Order of execution of various modules
#######################################

set ExecutionPath {

  ParticlePropagator


  ChargedHadronEfficiency
  ElectronEfficiency
  MuonEfficiency

  ElectronsMomentumSmearing
  MuonsMomentumSmearing
  ChargedHadronMomentumSmearing
    
  TrackMerger

  PhotonEnergySmearing
  Calorimeter 
  PhotonEfficiency
  
  FinalTrackMerger

}



#################################
# Propagate particles in cylinder
#################################

module PhotonConversions PhotonConversions {
   set InputArray Delphes/stableParticles

   set OutputArray stableParticles

   set Radius 3.31
   set HalfLength 12.0
   set EtaMin 2.0
   set EtaMax 5.0

   set Step 0.05
    set ConversionMap {  (abs(z) > 0.0 && abs(z) < 12.0 ) *     (0.07) +
	(abs(z) > 0.0) * (0.00) +
	(abs(z) < 0.0) * (0.00)
    }

}



module ParticlePropagatorLHCb ParticlePropagator {
  set InputArray Delphes/stableParticles

  set OutputArray stableParticles
  set ChargedHadronOutputArray chargedHadrons
  set ElectronOutputArray electrons
  set MuonOutputArray muons
  set NeutralOutputArray neutrals
  set PhotonOutputArray photons

  set OutputUpstreamArray upstreamParticles
  set ChargedHadronOutputUpstreamArray upstreamchargedHadrons
  set ElectronOutputUpstreamArray upstreamelectrons
  set MuonOutputUpstreamArray upstreammuons

  set OutputDownstreamArray downstreamParticles
  set ChargedHadronOutputDownstreamArray downstreamchargedHadrons
  set ElectronOutputDownstreamArray downstreamelectrons
  set MuonOutputDownstreamArray downstreammuons

  # radius of the magnetic field coverage, in m

  set Radius 3.31


  # half-length of the magnetic field coverage, in m
  set HalfLength 12

  # magnetic field
  set Bz 1.1

  set x0 3.0
  set y0 3.0

  # Need to veto anything with theta > 0.269 rad  -> eta = 2
  #                            theta < 0.0135 rad -> eta = 5

  # tracker and calos are at approx 0.269 rad, R = 12*tan(0.269)

}










########################################                                                                                                                                                                                                                                     
# Efficiency for charged hadrons                                                                                                                                                                                                                                             
########################################                                                                                                                                                                                                                                     
module EfficiencyHisto ChargedHadronEfficiency {
    set InputArray ParticlePropagator/chargedHadrons
    set OutputArray chargedHadrons
    set EfficiencyHisto \$LBDELPHESROOT/options/Brunel-2000ev-histos.root
}
########################################                                                                                                                                                                                                                                     
# Efficiency for electrons                                                                                                                                                                                                                                             
########################################                                                                                                                                                                                                                                     
module EfficiencyHisto ElectronEfficiency {
    set InputArray ParticlePropagator/electrons
    set OutputArray electrons
    set EfficiencyHisto \$LBDELPHESROOT/options/Brunel-2000ev-histos.root
}

########################################                                                                                                                                                                                                                                     
# Efficiency for muons
########################################                                                                                                                                                                                                                                     
module EfficiencyHisto MuonEfficiency {
    set InputArray ParticlePropagator/muons
    set OutputArray muons
    set EfficiencyHisto \$LBDELPHESROOT/options/Brunel-2000ev-histos.root
}


########################################
# Momentum resolution for charged tracks
########################################

module MomentumSmearingHisto ElectronsMomentumSmearing {
    
    set InputArray ElectronEfficiency/electrons
    set OutputArray electrons
    # set ResolutionFormula {resolution formula as a function of eta and pt}    
    # resolution formula for charged hadrons    
    #set ResolutionHisto /afs/cern.ch/user/b/bsiddi/cmtuser/GaussDev_v48r3/Sim/LbDelphes/options/ResSparse.root
    set ResolutionHisto \$LBDELPHESROOT/options/ResSparse.root
}

########################################
# Momentum resolution for charged tracks
########################################

module MomentumSmearingHisto MuonsMomentumSmearing {
    
    set InputArray MuonEfficiency/muons
    set OutputArray muons
    # set ResolutionFormula {resolution formula as a function of eta and pt}    
    # resolution formula for charged hadrons    
    set ResolutionHisto \$LBDELPHESROOT/options/ResSparse.root
}


########################################
# Momentum resolution for charged tracks
########################################

module MomentumSmearingHisto ChargedHadronMomentumSmearing {
    
    set InputArray ChargedHadronEfficiency/chargedHadrons
    set OutputArray chargedHadrons
    # set ResolutionFormula {resolution formula as a function of eta and pt}    
    # resolution formula for charged hadrons    
    set ResolutionHisto \$LBDELPHESROOT/options/ResSparse.root
}




###############
## Track merger
###############

module Merger TrackMerger {
# add InputArray InputArray
#    add InputArray ChargedHadronEfficiency/chargedHadrons
#    add InputArray ElectronEfficiency/electrons
#    add InputArray MuonEfficiency/muons

    add InputArray ElectronsMomentumSmearing/electrons
    add InputArray MuonsMomentumSmearing/muons
    add InputArray ChargedHadronMomentumSmearing/chargedHadrons
    add InputArray ParticlePropagator/photons
    add InputArray ParticlePropagator/neutrals
#    add InputArray ParticlePropagator/chargedHadrons
#    add InputArray ParticlePropagator/electrons
#    add InputArray ParticlePropagator/muons
    
# add InputArray ElectronEnergySmearing/electrons
# add InputArray MuonMomentumSmearing/muons
  set OutputArray tracks
}


module EnergySmearing PhotonEnergySmearing {  
   set InputArray ParticlePropagator/stableParticles
   set OutputArray SmearParticles

    set ResolutionFormula {
	sqrt(energy^2*0.107^2 + energy*2.08^2)
    }

}



module CalorimeterLHCb Calorimeter {
#   set ParticleInputArray PhotonEnergySmearing/SmearParticles 
   #set ParticleInputArray ParticlePropagator/stableParticles
   set ParticleInputArray ParticlePropagator/allParticles
   set TrackInputArray TrackMerger/tracks

   set TowerOutputArray ecalTowers
   set PhotonOutputArray photonArray
   
   set EFlowTrackOutputArray eflowTracks
   set EFlowTowerOutputArray eflowTower
   set EFlowPhotonOutputArray eflowPhoton
   set EFlowNeutralHadronOutputArray eflowNeutralHadrons
   set CaloBoarders caloBoarders
   set SmearTowerCenter false
#   set IsEcal true

   set ZECal 12000.

   set ECalEnergyMin 0
   set HCalEnergyMin 0
 
   set ECalEnergySignificanceMin 1.0
   set HCalEnergySignificanceMin 1.0

   set SmearTowerCenter true

   set pi [expr {acos(-1)}]


   set BorderX [list 323.2 969.6 1939.2 3878.4]
   set BorderY [list 323.2 727.2 1212.0 3151.2]
 
   set YBins {}
   for {set i 0} {$i <= 36} {incr i} {
       add YBins [expr { -727.2 + $i * 40.4 }]
   } 
   for {set i 0} {$i <= 48} {incr i} {
       set X [expr { -969.6 + $i * 40.4 }]
       add XYBins1 $X $YBins
   }

   set YBins {}
   for {set i 0} {$i <= 40} {incr i} {
       add YBins [expr {-1212.0 + $i * 60.6 }]
   } 
   for {set i 0} {$i <= 64} {incr i} {
       set X [expr {-1939.2 + $i * 60.6 }]
       add XYBins2 $X $YBins
   }

   set YBins {}
   for {set i 0} {$i <= 52} {incr i} {
       add YBins [expr {-3151.2 + $i * 121.2 }]
   } 
   for {set i 0} {$i <= 64} {incr i} {
       set X [expr {-3878.4 + $i * 121.2 }]
       add XYBins3 $X $YBins
   }



   add EnergyFraction {0} {0.0 1.0}
   add EnergyFraction {11} {1.0 0.0}
   add EnergyFraction {22} {1.0 0.0}
   #add EnergyFraction {111} {1.0 0.0}
   #add EnergyFraction {211} {0.0 0.0}
   #add EnergyFraction {321} {0.0 0.0}
   add EnergyFraction {12} {0.0 0.0}
   add EnergyFraction {13} {0.0 0.0}
   add EnergyFraction {14} {0.0 0.0}
   add EnergyFraction {16} {0.0 0.0}
   #add EnergyFraction {2212} {0.0 0.0}

   add EnergyFraction {1000022} {0.0 0.0}
   add EnergyFraction {1000023} {0.0 0.0}
   add EnergyFraction {1000025} {0.0 0.0}
   add EnergyFraction {1000035} {0.0 0.0}
   add EnergyFraction {1000045} {0.0 0.0}


   add EnergyFraction {310} {0.3 0.7}
   add EnergyFraction {3122} {0.3 0.7}
   
   set ECalResolutionFormula {(eta<=6.0 && eta > 1.0) * sqrt(energy^2*0.01^2 + 1000*energy*0.10^2 )}
   set HCalResolutionFormula {(eta<=6.0 && eta > 1.0) * sqrt(energy^2*0.01^2 + 1000*energy*0.10^2 )}


}




module Efficiency PhotonEfficiency {
   set InputArray Calorimeter/eflowPhoton 
   set OutputArray EffPhotons
}

module Merger FinalTrackMerger {
    add InputArray TrackMerger/tracks
    add InputArray Calorimeter/eflowPhoton
    #add InputArray Calorimeter/eflowNeutralHadrons
    set OutputArray tracks
}

