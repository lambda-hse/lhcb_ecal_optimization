################################################################################
# Package: GaussTracker
################################################################################
gaudi_subdir(GaussTracker v7r0p1)

gaudi_depends_on_subdirs(Sim/GaussTools)

find_package(Boost)
find_package(CLHEP)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})

gaudi_add_module(GaussTracker
                 src/*.cpp
                 LINK_LIBRARIES GaussToolsLib)

gaudi_env(SET GAUSSTRACKEROPTS \${GAUSSTRACKERROOT}/options)
